﻿//! Copyright (c) 2014, Mads Klinkby All rights reserved.

(function (define) {
    "use strict";

    define(["layouts/sp", "moment", "ng"], function (SP, moment) {
        var factory = ["$q", "$window", function ($q, $window) {
            var storage = $window.sessionStorage,
                key = "license",
                productId,
                deferred;

            function setLicenseValid() {
                storage.setItem(key, productId);
                deferred.resolve();
            }

            function setLicenseInvalid(reason) {
                storage.setItem(key, reason);
                deferred.reject(reason);
            }

            function getXmlElementText(elementName, xml) {
                var regExp = new RegExp("(?:\\<" + elementName + "\\>\s*)([\\w\\W]+?)(?:\\s*\\<\\/" + elementName + "\\>)"),
                    match = regExp.exec(xml);
                if (null === match) return null;
                return match[1];
            }          
          
            function validateLicenseToken(xmlToken) {
                //xmlToken = '<VerifyEntitlementTokenResponse xmlns="http://schemas.datacontract.org/2004/07/Microsoft.OfficeMarketplace.ServicePlatform.VerificationAgent.Service.Contract" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><AssetId>WA103786424</AssetId><DeploymentId>{987601H2-UY69-4593-B923-OL6381PC8CV2}</DeploymentId><EntitlementAcquisitionDate>2012-11-09T21:19.39Z</EntitlementAcquisitionDate><EntitlementExpiryDate i:nil="true" /><EntitlementType>Paid</EntitlementType><IsEntitlementExpired>false</IsEntitlementExpired><IsExpired>false</IsExpired><IsSiteLicense>false</IsSiteLicense><IsTest>false</IsTest><IsValid>true</IsValid><ProductId>{123601F2-AC69-1298-B9FC-CV6301DC8NM6}</ProductId><Seats>1</Seats><SignInDate>2013-09-21T00:00:99Z</SignInDate><TokenExpiryDate>2013-10-23T21:02:32Z<TokenExpiryDate><UserId>ddd63edd8ndsndswdn3k=</UserId>';
                var licenseProductId = getXmlElementText("ProductId", xmlToken),
                    isValid = "true" === getXmlElementText("IsValid", xmlToken),
                    entitlementType = getXmlElementText("EntitlementType", xmlToken),
                    expiryDate;
                if (licenseProductId !== productId || !isValid) {
                    setLicenseInvalid("Invalid license");
                    return;
                }
                if ("Trial" === entitlementType) {
                    expiryDate = moment(getXmlElementText("EntitlementExpiryDate", xmlToken)).toDate();
                    if (expiryDate < new Date()) {
                        setLicenseInvalid("Unfortunately your trial has expired. Purchase a license for the app in the Office store to keep using using it.");
                        return;
                    }
                    setLicenseValid();
                    return;
                }
                if ("Paid" === entitlementType || "Free" === entitlementType) {
                    setLicenseValid();
                    return;
                }
                setLicenseInvalid("Unexpected license token.");
            }

            function validateLicenseCollection(licenses) {
                var topLicense,
                    encodedTopLicense,
                    proxyRequest,
                    proxyResponse,
                    ctx;

                if (0 === licenses.get_count()) {
                    setLicenseInvalid("No license");
                    return;
                }
                topLicense = licenses.get_item(0).get_rawXMLLicenseToken();
                encodedTopLicense = encodeURIComponent(topLicense);

                proxyRequest = new SP.WebRequestInfo();
                proxyRequest.set_url("https://verificationservice.officeapps.live.com/ova/verificationagent.svc/rest/verify?token=" + encodedTopLicense);
                proxyRequest.set_method("GET");
                ctx = SP.ClientContext.get_current();
                proxyResponse = SP.WebProxy.invoke(ctx, proxyRequest);
                ctx.executeQueryAsync(function() {
                    var xmlToken = proxyResponse.get_body();
                    if (null === xmlToken) {
                        setLicenseInvalid("Got an empty response from Office Verification Service");
                        return;
                    }
                    validateLicenseToken(xmlToken);
                }, function() {
                    setLicenseInvalid("Could not get license information from Office Verification Service");
                });
            }

            function fetchAppLicenseInfo() {
                var ctx = SP.ClientContext.get_current(),
                    licenses = SP.Utilities.Utility.getAppLicenseInformation(ctx, productId);
                ctx.executeQueryAsync(function () {
                    validateLicenseCollection(licenses);
                 }, function() {
                    setLicenseInvalid("Could not get license information from SharePoint");
                });
            }

            function validate(appProductId) {
                var value = storage.getItem(key);
                productId = appProductId;
                deferred = $q.defer();
                if (productId === value) {
                    setLicenseValid();
                } else if (!value) {
                    fetchAppLicenseInfo();
                } else {
                    setLicenseInvalid(value);
                }
                return deferred.promise;
            };

            return {
                validate: validate
            };
        }];
        return factory;
    });

}(window.define));
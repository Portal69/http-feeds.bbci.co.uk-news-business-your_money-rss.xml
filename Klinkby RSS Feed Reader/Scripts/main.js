﻿//! Copyright (c) 2014, Mads Klinkby All rights reserved.
/*jslint browser: true, plusplus: true, regexp: true, white: true */
(function (window, document, require) {
    "use strict";

    var webUrl = /[^\/](\/[^\/][\w\W]+?)(?:\/Pages\/)/i.exec(window.location.href)[1],
        appName = "app";


    require.config({
        baseUrl: webUrl + "/Scripts",
        paths: {
            "layouts": "//cdn.sharepointonline.com/7585/_layouts/15",
            ng: "lib/angular/1.2.8/angular.min",
            "ngResource": "lib/angular/1.2.8/angular-resource",
            moment: "lib/moment/2.5.0/moment.min"
        },
        shim: {
            "layouts/sp": {
                deps: ["layouts/sp.runtime"],
                exports: "SP"
            },
            "layouts/sp.runtime": {
                deps: ["layouts/sp.init"]
            },
            "layouts/sp.init": {
                deps: ["layouts/init", "layouts/microsoftajax"]   
            },
            ngResource: {
                deps: ["ng"]
            },
            ng: {
                exports: "angular"
            }
        }
    });
        
    require(["ng", appName], function (ng) {

        function main() {
            ng.bootstrap(document, [appName]);
        }

        // wait or bind now if ready
        if ("loading" === document.readyState) {
            ng.element(document).ready(main);
        } else {
            main();
        }
    });

}(window, window.document, window.require));
